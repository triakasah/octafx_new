<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="OctaFX Indonesia" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,900" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<!-- Bootstrap Switch CSS -->
	<link rel="stylesheet" href="css/components/bs-switches.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=7acdf4" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/seo/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/seo/seo.css" type="text/css" />
	<link rel="stylesheet" href="css/calendar.css" type="text/css" />
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Broker Forex ECN, Trading Forex Online di Indonesia - OctaFX</title>
	<meta name="description" content="Trading Forex dengan kondisi dan Aplikasi Trading terbaik: Perlindungan Balance Negatif, Tanpa Swap, Tanpa Komisi, Bonus 50% pada tiap Deposit!"/>
	<meta name="keywords" content="forex, forex trading, broker, currency trading, deposit, bonus, forex broker, no requote, no deposit bonus, deposit bonus, visa, mastercard"/>
	<!-- Open Graph Data -->
	<meta property="og:url" content="https://www.octafx.forex/"/>
	<meta property="og:title" content="Broker Forex ECN, Trading Forex Online di Indonesia - OctaFX"/>
	<meta property="og:description" content="Trading Forex dengan kondisi dan Aplikasi Trading terbaik: Perlindungan Balance Negatif, Tanpa Swap, Tanpa Komisi, Bonus 50% pada tiap Deposit!"/>
	<meta property="og:image" content="https://www.octafx.forex/preview/default.png?db0e93910f5ed64fdcfcc77a12ff5f98b01bd346"/>
	<meta property="og:type" content="website"/>

	<meta property="og:site_name" content="OctaFX">
	<meta property="twitter:image" content="https://www.octafx.forex/preview/default.png?db0e93910f5ed64fdcfcc77a12ff5f98b01bd346">
	<meta property="twitter:url" content="https://www.octafx.forex/">
	<meta property="twitter:card" content="summary">
	<meta property="twitter:title" content="Broker Forex ECN, Trading Forex Online di Indonesia - OctaFX">
	<meta property="twitter:description" content="Trading Forex dengan kondisi dan Aplikasi Trading terbaik: Perlindungan Balance Negatif, Tanpa Swap, Tanpa Komisi, Bonus 50% pada tiap Deposit!">
	<meta itemprop="image" content="https://www.octafx.forex/preview/default.png?db0e93910f5ed64fdcfcc77a12ff5f98b01bd346">
	<meta itemprop="description" content="Trading Forex dengan kondisi dan Aplikasi Trading terbaik: Perlindungan Balance Negatif, Tanpa Swap, Tanpa Komisi, Bonus 50% pada tiap Deposit!">
	<meta itemprop="url" content="https://www.octafx.forex/">
	<meta name="twitter:site" content="@OctaFX">

	<link rel="shortcut icon" sizes="16x16" href="https://www.octafx.forex/favicon.ico" />
    <link rel="shortcut icon" sizes="32x32" href="https://www.octafx.forex/favicon-32x32.ico" />
    <link rel="icon" sizes="192x192" href="https://www.octafx.forex/android-192x192.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.octafx.forex/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://www.octafx.forex/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.octafx.forex/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.octafx.forex/apple-touch-icon-180x180.png" />
    <link rel="apple-touch-icon" sizes="167x167" href="https://www.octafx.forex/apple-touch-icon-167x167.png" />

</head>

<body class="stretched" data-loader-html='<img class="preloader-video" src="logo_white.gif" alt="">'>
<!-- <body class="stretched" data-loader-html='<video class="preloader-video" src="logo_white.mp4" autobuffer autoloop loop autoplay></video>'> -->

<div class="modal1 mfp-hide subscribe-widget" id="myModal1">
	<div class="block dark divcenter" style="background: url('images/modals/modal1.jpg') no-repeat; background-size: cover; max-width: 700px;" data-height-xl="400">
		<div style="padding: 50px; color:#444">
			<div class="heading-block nobottomborder bottommargin-sm" style="max-width:500px;">
				<h3 style="color: #444">Hubungi Kembali</h3>
				<span style="color: #444">Kami akan segera menghubungi Anda</span>
			</div>
			<div class="widget-subscribe-form-result"></div>
			<form class="widget-subscribe-form2" action="include/subscribe.php" role="form" method="post" style="max-width: 350px;">
				<input type="number" id="widget-subscribe-form2-email" name="widget-subscribe-form-email" class="form-control form-control-lg not-dark required" placeholder="Masukkan nomor HP Anda">
				<button class="button button-rounded button-border button-light noleftmargin" type="submit" style="border-color: #444;color: #444;margin-top:15px;">Hubungi Saya</button>
			</form>
		</div>
	</div>
</div>
</div>
</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
		<div id="top-bar" class="transparent-topbar">

			<div class="container clearfix">

				<div class="col_half nobottommargin clearfix">

					<!-- Top Links
					============================================= -->
					<div class="top-links">
						<ul>
							<li><a href="#"><img src="demos/seo/images/flags/id.png" alt="Lang">IDN</a>
								<ul>
									<li><a href="#"><img src="demos/seo/images/flags/eng.png" alt="Lang">Eng</a></li>
									<li><a href="#"><img src="demos/seo/images/flags/fre.png" alt="Lang">Fre</a></li>
									<li><a href="#"><img src="demos/seo/images/flags/ara.png" alt="Lang">Ara</a></li>
									<li><a href="#"><img src="demos/seo/images/flags/tha.png" alt="Lang">Tha</a></li>
								</ul>
							</li>
							<!-- <li><a href="#">Deposit & Withdraw</a></li> -->
							<!-- <li class="hide-mobile"><a href="#">Kemitraan</a></li>
							<li class="hide-mobile"><a href="#">F.A.Q</a></li> -->
							<li><a  href="#myModal1" data-lightbox="inline"><i class="icon-phone3"></i>Telpon Kembali</a></li>
						</ul>
					</div><!-- .top-links end -->

				</div>

				<div class="col_half fright dark col_last clearfix nobottommargin">

					<!-- Top Social
					============================================= -->
					<div id="top-social">
						<ul>
							<li><a href="https://www.facebook.com/octafx.idn" class="si-facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="https://twitter.com/OctaFX" class="si-twitter" target="_blank"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
							<li><a href="https://www.youtube.com/user/octafx" class="si-youtube" target="_blank"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
							<li><a href="https://www.instagram.com/octafx_official/" class="si-instagram" target="_blank"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
							<li><a href="tel:+622131106972" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+62 21 3110 6972</span></a></li>
							<li><a href="mailto:support@octafx.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">support@octafx.com</span></a></li>
							<li><a href="#" class="si-user"><span class="ts-icon"><i class="icon-user4"></i></span><span class="ts-text">Masuk</span></a></li>
						</ul>
					</div><!-- #top-social end -->


				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header floating-header clearfix" data-sticky-class="not-dark">

			<div id="header-wrap">

				<div class="container clearfix gradient-bg">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="#" class="standard-logo" data-dark-logo="https://static-markup.octaglobal.com/common/components-frontsite/header.v1/assets/img/pg-logo-octafx.svg?d0b780f8481ef759b062f463f1b15ef3"><img src="https://static-markup.octaglobal.com/common/components-frontsite/header.v1/assets/img/pg-logo-octafx.svg?d0b780f8481ef759b062f463f1b15ef3" alt="OctaFX Logo"></a>
						<a href="#" class="retina-logo" data-dark-logo="https://static-markup.octaglobal.com/common/components-frontsite/header.v1/assets/img/pg-logo-octafx.svg?d0b780f8481ef759b062f463f1b15ef3"><img src="https://static-markup.octaglobal.com/common/components-frontsite/header.v1/assets/img/pg-logo-octafx.svg?d0b780f8481ef759b062f463f1b15ef3" alt="OctaFX Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="with-arrows">

						<ul>
							<li class="current mega-menu submenu"><a href="#"><div>Trading FX</div></a>
							<div class="mega-menu-content clearfix">
								<ul class="mega-menu-column col">
									<center class="hide-mobile ">
										<img src="images/mega-menu-1.png" alt="Mega Menu 1">
									</center>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Deposit & Withdraw</div></a></li>
									<li><a href="#"><div>Opsi Pembayaran</div></a></li>
									<li><a href="#"><div>Keamanan Dana</div></a></li>
									<li><a href="#"><div>Perlindungan Dana Negatif</div></a></li>
									<li><a href="#"><div>Deposit Akun Saya</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Platform Trading</div></a></li>
									<li><a href="#"><div>MetaTrader 4</div></a></li>
									<li><a href="#"><div>MetaTrader 5</div></a></li>
									<li><a href="#"><div>MetaTrader Web</div></a></li>
									<li><a href="#"><div>cTrader</div></a></li>
									<li><a href="#"><div>OctaFX Trading</div></a></li>
									<li><a href="#"><div>OctaFX Copytrading</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Copytrading</div></a></li>
									<li><a href="#"><div>Bergabung di Copytrading</div></a></li>
									<li><a href="#"><div>Menjadi Master</div></a></li>
									<li><a href="#"><div>Syarat dan Ketentuan</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Instrumen</div></a></li>
									<li><a href="#"><div>Trading CFD</div></a></li>
									<li><a href="#"><div>Cryptocurrencies</div></a></li>
									<li><a href="#"><div>Trading Komoditas</div></a></li>
									<li><a href="#"><div>Trading Saham</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Ketentuan</div></a></li>
									<li><a href="#"><div>Akun Trading </div></a></li>
									<li><a href="#"><div>Akun Islami </div></a></li>
									<li><a href="#"><div>Akun Demo </div></a></li>
									<li><a href="#"><div>Spread</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Alat Trader</div></a></li>
									<li><a href="#"><div>Kalender Ekonomi </div></a></li>
									<li><a href="#"><div>Kalkulator Trading </div></a></li>
									<li><a href="#"><div>Kalkulator Profit </div></a></li>
									<li><a href="#"><div>Pemantauan </div></a></li>
									<li><a href="#"><div>Autochartist</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Analisa</div></a></li>
									<li><a href="#"><div>Wawasan Market </div></a></li>
									<li><a href="#"><div>Berita Forex </div></a></li>
									<li><a href="#"><div>Suku Bunga </div></a></li>
									<li><a href="#"><div>Libur Nasional </div></a></li>
									<li><a href="#"><div>Live Quotes </div></a></li>
									<li><a href="#"><div>Market in a Minute</div></a></li>
								</ul>
							</div>
							</li>

							<li class="mega-menu"><a href="#"><div>Promosi dan Kemitraan</div></a>
							<div class="mega-menu-content clearfix">
								<ul class="mega-menu-column col">
									<center class="hide-mobile ">
										<img src="images/mega-menu-2.png" alt="Mega Menu 1">
									</center>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Kontes</div></a></li>
									<li><a href="#"><div>Kontes Rill Supercharged 2 </div></a></li>
									<li><a href="#"><div>Kontes Demo Champion </div></a></li>
									<li><a href="#"><div>Kontes Demo cTrader Weekly</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Penawaran dan Bonus</div></a></li>
									<li><a href="#"><div>Kurs Rupiah Tetap </div></a></li>
									<li><a href="#"><div>Hadiah Sepeda Motor Mingguan </div></a></li>
									<li><a href="#"><div>Trade & Win </div></a></li>
									<li><a href="#"><div>Penawaran Ramdan </div></a></li>
									<li><a href="#"><div>Bonus Deposit 100%</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<center class="hide-mobile ">
										<img src="images/mega-menu-2-1.png" alt="Mega Menu 1">
									</center>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Kemitraan (Afiliasi)</div></a></li>
									<li><a href="#"><div>Program Afiliasi </div></a></li>
									<li><a href="#"><div>Undang Teman </div></a></li>
									<li><a href="#"><div>Program IB </div></a></li>
									<li><a href="#"><div>Master IB </div></a></li>
									<li><a href="#"><div>Bonus Bulanan IB </div></a></li>
									<li><a href="#"><div>Kontes Supercharged 2 IB</div></a></li>
								</ul>
							</div>
							</li>

							<li class="mega-menu"><a href="#"><div>Edukasi</div></a>
							<div class="mega-menu-content clearfix">
								<ul class="mega-menu-column col">
									<center class="hide-mobile ">
										<img src="images/mega-menu-3.png" alt="Mega Menu 1">
									</center>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Dasar Dasar Forex</div></a></li>
									<li><a href="#"><div>Cara Memulai Trading </div></a></li>
									<li><a href="#"><div>Strategi Trading </div></a></li>
									<li><a href="#"><div>Cara Trading Forex </div></a></li>
									<li><a href="#"><div>Analisa Market </div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Tutorial</div></a></li>
									<li><a href="#"><div>Video Tutorial </div></a></li>
									<li><a href="#"><div>Cara Memulai Trading </div></a></li>
									<li><a href="#"><div>Cara Melakukan Deposit </div></a></li>
									<li><a href="#"><div>Cara Membuka Akun Trading</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">F.A.Q</div></a></li>
									<li><a href="#"><div>Pembukaan Akun </div></a></li>
									<li><a href="#"><div>Deposit dan Withdraw </div></a></li>
									<li><a href="#"><div>Kondisi Trading </div></a></li>
									<li><a href="#"><div>Akun Trading</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">OcBlog</div></a></li>
									<li><a href="#"><div>Blog Forex </div></a></li>
									<li><a href="#"><div>Mendapatkan Uang Dari Forex </div></a></li>
									<li><a href="#"><div>Langkah Menjadi Trader Sukses </div></a></li>
									<li><a href="#"><div>Mengapa Trader Kehilangan Uang</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Seminar</div></a></li>
									<li><a href="#"><div>Jadwal Seminar </div></a></li>
									<li><a href="#"><div>Topik Seminar </div></a></li>
									<li><a href="#"><div>Pembicara Professional</div></a></li>
									<li><a href="#"><div>Dokumentasi Seminar</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Trading Tiger Pro</div></a></li>
									<li><a href="#"><div>Kursus Online Dasar Forex </div></a></li>
								</ul>
							</div>
							</li>

							<li class="mega-menu"><a href="#"><div>Tentang OctaFX</div></a>
							<div class="mega-menu-content clearfix">
								<ul class="mega-menu-column col">
									<center class="hide-mobile ">
										<img src="images/mega-menu-4.png" alt="Mega Menu 1">
									</center>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">OctaFX</div></a></li>
									<li><a href="#"><div>Mengapa OctaFX </div></a></li>
									<li><a href="#"><div>Penghargaan </div></a></li>
									<li><a href="#"><div>Layanan Pelanggan </div></a></li>
									<li><a href="#"><div>Berita </div></a></li>
									<li><a href="#"><div>Video</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Penghargaan dan Lisensi</div></a></li>
									<li><a href="#"><div>Broker Forex Terbaik Se-Asia </div></a></li>
									<li><a href="#"><div>Platform Copy Trading Terbaik </div></a></li>
									<li><a href="#"><div>Most Transparent Broker </div></a></li>
									<li><a href="#"><div>Lisensi CySEC</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Sponsorship</div></a></li>
									<li><a href="#"><div>RIP CURL CUP </div></a></li>
									<li><a href="#"><div>Southampton FC </div></a></li>
									<li><a href="#"><div>PERSIB Bandung FC</div></a></li>
									<div class="mega-menu-separator hide-mobile"></div>
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Tanggung Jawb Sosial</div></a></li>
									<li><a href="#"><div>Bali Sports Foundation </div></a></li>
									<li><a href="#"><div>Kegiatan Amal</div></a></li>
								</ul>
								<ul class="mega-menu-column mega-menu-subtitle col">
									<li class="mega-menu-subtitle"><a href="#"><div class="subheading">Hubungi Kami</div></a></li>
									<li><a href="#"><div>LiveChat</div></a></li>
									<li><a href="#"><div>Facebook</div></a></li>
									<li><a href="#"><div>Instagram</div></a></li>
									<li><a href="#"><div>Twitter</div></a></li>
									<li><a href="#"><div>Email</div></a></li>
									<li><a href="#"><div>Hubungi Kembali</div></a></li>
								</ul>
							</div>
							</li>


						</ul>


							<!-- Top Search
							============================================= -->
							<div id="top-search" class="fleft">
								<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
								<form action="search.html" method="get">
									<input type="text" name="q" class="form-control" value="" placeholder="Ketik &amp; enter..">
								</form>
							</div><!-- #top-search end -->

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element full-screen clearfix" style="background: #FFF url('demos/seo/images/hero/hero-1.jpg') center center no-repeat; background-size: cover;">

			<div class="vertical-middle">
				<div class="container topmargin-sm">
					<div class="row">
						<div class="col-lg-5 col-md-8 slider-caption-cust">
							<div class="slider-title">
								<!-- <div class="badge badge-pill badge-default">SEO Ready</div> -->
								<div class="card shadow-lg">
									<div class="card-body">
										<h2 class="mb-3 slider-title-cust">Broker Forex dan CFD terdepan di Indonesia</h2>
										<div class="contact-widget">
											<div class="contact-form-result"></div>
											<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">
												<div class="col_full mb-3">
													<label for="template-contactform-name">Nama Lengkap:*</label>
													<input type="text" id="template-contactform-name" name="template-contactform-name" class="form-control input-sm required" value="">
												</div>
												<div class="col_full mb-3">
													<label for="template-contactform-email">Alamat Email:*</label>
													<input type="email" id="template-contactform-email" name="template-contactform-email" class="form-control input-sm required" value="">
												</div>
												<div class="col_full mb-4">
													<label for="template-contactform-website">Kata Sandi:*</label>
													<input type="text" id="template-contactform-website" name="template-contactform-website" class="form-control input-sm required" value="">
												</div>
												<div class="col_full nobottommargin">
													<button class="button button-rounded btn-block nott ls0 m-0 button-hover" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Buka Akun Sekarang</button>
												</div>
											</form>
										</div>
									</div>
									<div class="card-footer">
										<h3 class="text-rotater mb-2" data-separator="," data-rotate="fadeIn" data-speed="3500">- Hanya di OctaFX <span class="t-rotate">Bonus Deposit 100%,Berbagai Hadiah,Deposit Bank Lokal</span> Bisa Anda Dapatkan!</h3>
										<p>Trading Forex dan CFD dengan spread terendah.</p>
										<!-- <a href="#" class="button button-rounded button-large nott ls0">Get Started</a>
										<a href="#" class="button button-rounded button-large button-light text-dark bg-white border nott ls0">Contact Us</a> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="video-wrap h-100 d-block d-lg-none">
				<div class="video-overlay" style="background: rgba(255,255,255,0.85);"></div>
			</div>


		</section><!-- #slider end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap pt-0">

				<!-- Spread and Promotion
				============================================= -->
				<!-- <div class="section mt-0" style="background: url('demos/seo/images/sections/4.png') no-repeat center top; background-size: cover; padding: 0px 0 0;"> -->
				<div class="section mt-0 pb-0 mb-0 parallax" style="margin-bottom: 0px;background: url('demos/seo/images/sections/4.png') no-repeat center top; background-size: cover;">
					<div class="container">
						<div class="row">
							<div class="col mb-3">
								<div class="feature-box fbox-center fbox-border fbox-dark fbox-effect noborder">
									<div class="fbox-icon">
										<a href="#"><i class="icon-dollar-sign i-alt"></i></a>
									</div>
									<h3>Minimal Deposit $5</h3>
									<p class="text-justify">Rasakan nikmatnya trading dengan fleksibilitas deposit, namun kami merekomendasikan deposit $50 agar lebih leluasa.</p>
								</div>
							</div>
							<div class="col mb-3">
								<div class="feature-box fbox-center fbox-border fbox-dark fbox-effect noborder">
									<div class="fbox-icon">
										<a href="#"><i class="icon-coins i-alt"></i></a>
									</div>
									<h3>Bonus Deposit 100%</h3>
									<p class="text-justify">Ada tambahan 100% untuk setiap deposit yang Anda lakukan, lebih banyak modal dan lebih banyak keuntungan untuk Anda.</p>
								</div>
							</div>
							<div class="col mb-3">
								<div class="feature-box fbox-center fbox-border fbox-dark fbox-effect noborder">
									<div class="fbox-icon">
										<a href="#"><i class="icon-motorcycle i-alt"></i></a>
									</div>
									<h3>Hadiah Sepeda Motor</h3>
									<p class="text-justify">Hanya disini Anda bisa trading dan tiba tiba bisa dapat motor! Dan hadiah ini juga dibagikan setiap minggunya.</p>
								</div>
							</div>
							<div class="col mb-3">
								<div class="feature-box fbox-center fbox-border fbox-dark fbox-effect noborder">
									<div class="fbox-icon">
										<a href="#"><i class="icon-gift i-alt"></i></a>
									</div>
									<h3>Trade &amp; Win</h3>
									<p class="text-justify">Hanya dengan trading saja Anda bisa mengklaim hadiah lainnya, tanpa diundi! Cukup trading seperti biasa saja.</p>
								</div>
							</div>
						</div>

					</div>
				</div>



				<!-- Start Trading
				============================================= -->
				<div class="section m-0" style="background: url('demos/seo/images/sections/bg-stock-gray-left.jpg') no-repeat center top; background-size: cover; padding: 0px 0 0;">
					<div class="container">
						<div class="row justify-content-between">
							<div class="col-md-12 mt-5">
								<div class="feature-box fbox-center noborder">
									<div class="row clearfix align-items-stretch bottommargin-lg">
										<div class="col-lg-3 col-md-3 col-6 dark center col-padding" style="background-color: #515875;">
											<h2>0.1</h2>
											<h5>Spread Terkini EURUSD</h5>
										</div>
										<div class="col-lg-3 col-md-3 col-6 dark center col-padding" style="background-color: #576F9E;">
											<h2>0.1</h2>
											<h5>Spread Terkini GBPUSD</h5>
										</div>
										<div class="col-lg-3 col-md-3 col-6 dark center col-padding" style="background-color: #6697B9;">
											<h2>0.1</h2>
											<h5>Spread Terkini USDJPY</h5>
										</div>
										<div class="col-lg-3 col-md-3 col-6 dark center col-padding" style="background-color: #88C3D8;">
											<h2>0.1</h2>
											<h5>Spread Terkini AUDUSD</h5>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 mt-4">
								<div class="heading-block nobottomborder bottommargin-sm">
									<h3 class="text-center nott ls0">Mulai Trading<br>Sesuai Dengan Ketentuan Anda</h3>
								</div>
								<p class="text-justify">OctaFX - salah satu broker Forex terbaik di market, untuk semua trader di seluruh dunia. OctaFX menawarkan akses trading CFD, Cryptocurrencies, komoditas dan indeks. OctaFX sudah mendapatkan berbagai penghargaan <i>trading condition, best forex broker, best copytrading platform</i> dan lainnya. Dalam akses ke pasar Forex, OctaFX juga menawarkan berbagai promosi yang dapat membantu Anda!</p>
							</div>

							<div class="col-lg-12 col-md-12">
								<div id="section-pricing" class="page-section nopadding nomargin">

									<div id="pricing-switch" class="pricing row align-items-end bottommargin-lg clearfix">

											<div class="col-md-3">

												<div class="pricing-box" style="background-color: rgba(25,118,210,.1);">
													<div class="pricing-title">
														<center>
															<h4>Pilihan Favorit</h4>
															<h3>MetaTrader 4</h3>
															<span>Direkomendasikan untuk Trader pemula, untuk investasi kecil dan resiko kecil</span>
														</center>
													</div>
													<div class="pricing-price">
														<center>
															<div class="pts-switch-content-left"><span class="price-unit">&dollar;</span>5<span class="price-tenure">Minimal Deposit</span></div>
														</center>
													</div>
													<div class="pricing-features border-bottom-0">
														<ul>
															<li><i class="icon-check-circle color mr-2"></i><strong>Spread:</strong> Mulai dari 0.4 pip</li>
															<li class="pts-switch-content-right"><i class="icon-check-circle color mr-2"></i><strong>Leverage</strong> Sampai 1:1000</li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Volume:</strong> 0.01 - tak terbatas</li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Akun Islami</strong></li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Trading CFD</strong></li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Trading Cryptocurrencies</strong></li>
														</ul>
													</div>
													<div class="pricing-action">
														<div class="pts-switch-content-right">
															<a href="#" class="button button-large button-rounded btn-block capitalize m-0 ls0 button-hover">Buka Akun</a>
															<br><br>
														</div>
													</div>
												</div>

											</div>
											<div class="col-md-3">

												<div class="pricing-box" style="background-color: rgba(2,136,209,.1);">
													<div class="pricing-title">
														<center>
															<h3>MetaTrader 5</h3>
															<span>Khusus untuk Trader berpengalaman, trading dengan spread rendah</span>
														</center>
													</div>
													<div class="pricing-price">
														<center>
															<div class="pts-switch-content-left"><span class="price-unit">&dollar;</span>500<span class="price-tenure">Minimal Deposit</span></div>
														</center>
													</div>
													<div class="pricing-features border-bottom-0">
														<ul>
															<li><i class="icon-check-circle color mr-2"></i><strong>Spread:</strong> Mulai dari 0.2 pip</li>
															<li class="pts-switch-content-right"><i class="icon-check-circle color mr-2"></i><strong>Leverage</strong> Sampai 1:200</li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Volume:</strong> 0.01 - tak terbatas</li>
															<li class="pts-switch-content-left text-black-50"><i class="icon-minus-circle mr-2"></i><del style="opacity: .5"><strong>Akun Islami</strong></del></li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Trading CFD</strong></li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Trading Cryptocurrencies</strong></li>
														</ul>
													</div>
													<div class="pricing-action">
														<div class="pts-switch-content-right">
															<a href="#" class="button button-large button-rounded btn-block capitalize m-0 ls0 button-hover">Buka Akun</a>
															<br><br>
														</div>
													</div>
												</div>

											</div>
											<div class="col-md-3" style="z-index: 2">

												<div class="pricing-box" style="background-color: rgba(0,151,167,.1);">
													<div class="pricing-title">
														<center>
															<h3>cTrader</h3>
															<span>direkomendasikan untuk trader progresif, lebih transparan dalam melihat harga</span>
														</center>
													</div>
													<div class="pricing-price">
														<center>
															<div class="pts-switch-content-left"><span class="price-unit">&dollar;</span>100<span class="price-tenure">Minimal Deposit</span></div>
														</center>
													</div>
													<div class="pricing-features border-bottom-0">
														<ul>
															<li><i class="icon-check-circle color mr-2"></i><strong>Spread:</strong> Mulai dari 0 pip</li>
															<li class="pts-switch-content-right"><i class="icon-check-circle color mr-2"></i><strong>Leverage</strong> Sampai 1:500</li>
															<li><i class="icon-check-circle color mr-2"></i><strong>Volume:</strong> 0.01 - tak terbatas</li>
															<li class="pts-switch-content-left text-black-50"><i class="icon-minus-circle mr-2"></i><del style="opacity: .5"><strong>Akun Islami</strong></del></li>
															<li class="pts-switch-content-left text-black-50"><i class="icon-minus-circle mr-2"></i><del style="opacity: .5"><strong>Trading CFD</strong></del></li>
															<li class="pts-switch-content-left text-black-50"><i class="icon-minus-circle mr-2"></i><del style="opacity: .5"><strong>Trading Cryptocurrencies</strong></del></li>
														</ul>
													</div>
													<div class="pricing-action">
														<div class="pts-switch-content-right">
															<a href="#" class="button button-large button-rounded btn-block capitalize m-0 ls0 button-hover">Buka Akun</a>
															<br><br>
														</div>
													</div>
												</div>
												
											</div>
											<div class="col-md-3 hide-mobile">
												<img src="demos/seo/images/sections/pic-01.svg" alt="" style="margin-bottom: 20%; left: -50%; max-width: 200%; position: relative; z-index: 1;">
											</div>

											<div class="col-md-9" style="margin-top: 0px;">
												<div class="pricing-action">
													<div class="pts-switch-content-right">
														<a href="#" class="button button-rounded button-large button-light bg-white border btn-block nott m-0 ls0 button-learn-more">Pelajari Perbandingan Akun</a>
														<br><br>
													</div>
												</div>
											</div>
									</div>

								</div>
							</div>

							<div class="col-md-12 mt-4 mb-5">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12 parallax mb-3" data-bottom-top="transform: translateY(-40px)" data-top-bottom="transform: translateY(40px)">
										<img src="demos/seo/images/sections/demo-acc-intro.svg" alt="" style="max-width: 100%;">
									</div>
									<div class="col-lg-6 col-md-6 col-12">
										<div class="heading-block nobottomborder bottommargin-sm">
											<h3 class="text-center nott ls0">Berlatih Trading Forex Dengan<br>Akun Demo OctaFX</h3>
										</div>
										
										<p class="text-justify"><strong>Belajar trading forex tanpa risiko sama sekali</strong> - Pasar Forex menawarkan <strong>peluang</strong> menarik bagi para trader, tetapi juga <strong>memiliki resiko</strong>. Itu sebabnya, sebelum Anda terjun ke dalam <strong>trading Forex</strong> dengan akun real, akan lebih baik jika Anda terlebih dahulu membuka <strong>akun demo</strong> yang bebas resiko. Akun Demo OctaFX memberikan pengalaman trading Forex yang sama seperti akun real, yang membedakan adalah dana yang ada di akun demo Forex merupakan <strong>dana virtual</strong> atau bukan dana sungguhan. Sehingga Anda tidak trading dengan uang sungguhan, namun keuntungan dan kerugian anda pun bukan sungguhan.</p>

										<a href="#" class="button button-large button-rounded capitalize ml-0 mt-5 ls0 button-hover btn-demo" style="position: absolute;">Buka Akun Demo</a>
										<h4>Mulai Latihan tanpa perlu deposit</h4>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

				<!-- Trading Platform, Deposit, and Why OctaFX
				============================================= -->
				<!-- <div class="section nobg mt-4 mb-0 pb-0"> -->
				<div class="section m-0 pt-5" style="background: url('demos/seo/images/sections/bg-stock-gray-left.jpg') no-repeat center top; background-size: cover; padding: 0px 0 0;">
					<div class="container">
						<div class="heading-block nobottomborder center divcenter mb-0 clearfix">
							<h3 class="nott ls0 mb-3">Platform Trading</h3>
							<p>Aksesibilitas Trading Forex untuk semua orang, berbisnis Trading Forex tidak pernah semudah ini sebelumnya!
							</p>
						</div>
						<div class="row justify-content-between align-items-center clearfix">

							<div class="col-12 col-lg-3 col-md-3">

								<div class="feature-box fbox-right noborder mobile-desc-r">
									<h3 class="nott ls0">OctaFX Trading App</h3>
									<p>Aplikasi OctaFX, Segera dapatkan kemudahan berbisnis trading forex dalam genggaman Anda!</p>
								</div>

								<div class="feature-box fbox-right noborder mt-5 mobile-desc-r">
									<h3 class="nott ls0">Kelola Semuanya</h3>
									<p>Atur semua akun trading, lakukan deposit dan withdraw, dan trading forex, semua dalam satu aplikasi</p>
								</div>

								<div class="mt-5 mobile-mt-1 mobile-desc-r">
									<a href="https://play.google.com/store/apps/details?id=com.octafx" class="download-devices-content__btn btn-wrap__btn btn-t34 -text-s02 -gplay -desktop-elem" target="_blank">
			                            <span class="btn-t34__inner">
			                                Dapatkan di <br>
			                                <img src="demos/seo/images/sections/icon-google-play-text.svg?c1ca4e84964eaa58af18733cb4af6ca55601ccf0" width="123">
	                                    </span>
			                        </a>
								</div>

							</div>

							<div class="col-6 offset-3 offset-sm-0 d-sm-none d-lg-block center my-5 hide-mobile">
								<img src="demos/seo/images/sections/background-section-3.png" alt="mobile" class="rounded  parallax" data-bottom-top="transform: translateY(-50px)" data-top-bottom="transform: translateY(50px)">
							</div>

							<div class="mobile-mt-3 col-12 col-lg-3 col-md-3">

								<div class="feature-box noborder mobile-desc-l">
									<h3 class="nott ls0">OctaFX Copytrading</h3>
									<p>Dapatkan keuntungan trader profesional dengan mengikuti setiap aksi tradingnya secara otomatis!</p>
								</div>

								<div class="feature-box noborder mt-5 mobile-desc-l">
									<h3 class="nott ls0">Platform Copytrading Terbaik</h3>
									<p>OctaFX Copytrading telah meraih berbagai penghargaan sebagai platform copytrading terbaik!</p>
								</div>

								<div class="feature-box noborder mobile-mt-1 mt-5 mobile-desc-l">
									<a href="https://play.google.com/store/apps/details?id=com.octafx.copytrade" class="download-devices-content__btn btn-wrap__btn btn-t34-green -text-s02 -gplay -desktop-elem" target="_blank">
			                            <span class="btn-t34__inner">
			                                Dapatkan di <br>
			                                <img src="demos/seo/images/sections/icon-google-play-text.svg?c1ca4e84964eaa58af18733cb4af6ca55601ccf0" width="123">
	                                    </span>
			                        </a>
								</div>

							</div>

						</div>
						<div class="acc-available mb-3 hide-mobile">
					        <div class="acc-available__title">
					            Platform Trading OctaFX sudah tersedia di:
					        </div>
					        <a href="/downloads" class="acc-available__item -desktop">
					            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve" class="software-platform-icon -desktop">
					                <g>
					                    <path class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="
					                        M27,22H3c-1.1,0-2-0.9-2-2V5c0-1.1,0.9-2,2-2h24c1.1,0,2,0.9,2,2v15C29,21.1,28.1,22,27,22z"></path>
					                    
					                    <line class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="20" y1="27" x2="10" y2="27"></line>
					                    <path style="fill:#777;" d="M29,18v1c0,1.7-1.3,3-3,3H4c-1.7,0-3-1.3-3-3v-1"></path>
					                </g>
					            </svg>
					        </a>
					        <a href="/downloads" class="acc-available__item -ios">
					            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve" class="software-platform-icon -ios">
					                <g>
					                    <path style="fill:#777;" class="software-platform-icon__elem" d="M23.3,16.1c0-3.8,3.1-5.6,3.3-5.6C24.8,7.9,22,7.6,21,7.5c-2.4-0.2-4.6,1.4-5.8,1.4c-1.2,0-3.1-1.3-5-1.3
					                        c-2.6,0-5,1.5-6.3,3.7c-2.7,4.5-0.7,11.3,1.9,14.9C7.1,28,8.6,30,10.6,30c1.9,0,2.7-1.2,5-1.2c2.3,0,3,1.2,5,1.2
					                        c2.1,0,3.4-1.8,4.7-3.6c1.5-2.1,2.1-4.1,2.1-4.2C27.3,22.1,23.3,20.6,23.3,16.1z"></path>
					                    <path style="fill:#777;" class="software-platform-icon__elem" d="M19.1,4.7c1-1.2,1.7-3,1.5-4.7c-1.5,0.1-3.3,1-4.4,2.2c-1,1.1-1.8,2.8-1.6,4.5C16.3,6.9,18,5.9,19.1,4.7z"></path>
					                </g>
					            </svg>
					        </a>
					        <a href="/downloads" class="acc-available__item -android">
					            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve" class="software-platform-icon -android">
					                <g>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M18.9,2.5l1.2-2c0.1-0.1,0.1-0.3-0.1-0.4C19.9,0,19.8,0,19.7,0.1l-1.3,2.1c-1-0.4-2.2-0.6-3.4-0.6
					                        c-1.2,0-2.4,0.2-3.4,0.6l-1.3-2.1C10.2,0,10,0,9.9,0.1C9.8,0.1,9.7,0.3,9.8,0.5l1.2,2C8.6,3.6,7,6.5,7,9h16
					                        C23,6.5,21.3,3.6,18.9,2.5z M11.6,6.8c-0.6,0-1.1-0.5-1.1-1.1c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1
					                        C12.8,6.3,12.3,6.8,11.6,6.8z M18.4,6.8c-0.6,0-1.1-0.5-1.1-1.1c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1
					                        C19.5,6.3,19,6.8,18.4,6.8z"></path>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M20.8,24H9.2C8,24,7,23,7,21.8L7,10h16v11.8C23,23,22,24,20.8,24z"></path>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M4,21L4,21c1.1,0,2-0.9,2-2v-7c0-1.1-0.9-2-2-2h0c-1.1,0-2,0.9-2,2v7C2,20.1,2.9,21,4,21z"></path>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M26,21L26,21c1.1,0,2-0.9,2-2v-7c0-1.1-0.9-2-2-2h0c-1.1,0-2,0.9-2,2v7C24,20.1,24.9,21,26,21z"></path>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M18,30L18,30c1.1,0,2-0.9,2-2v-7c0-1.1-0.9-2-2-2h0c-1.1,0-2,0.9-2,2v7C16,29.1,16.9,30,18,30z"></path>
					                    <path class="software-platform-icon__elem" style="fill:#777;" d="M12,30L12,30c1.1,0,2-0.9,2-2v-7c0-1.1-0.9-2-2-2h0c-1.1,0-2,0.9-2,2v7C10,29.1,10.9,30,12,30z"></path>
					                </g>
					            </svg>
					        </a>
					        <a href="/downloads" class="acc-available__item -web">
					            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve" class="software-platform-icon -web">
					                <g>
					                    <circle class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" cx="15" cy="15" r="13"></circle>
					                    <line class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="15" y1="2" x2="15" y2="28"></line>
					                    <line class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="2" y1="15" x2="28" y2="15"></line>
					                    <line class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="4.1" y1="9" x2="26.4" y2="9"></line>
					                    <ellipse class="software-platform-icon__elem" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" cx="15" cy="15" rx="6.8" ry="13"></ellipse>
					                    <line class="" style="fill:none;stroke:#777;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="4.1" y1="21" x2="26.4" y2="21"></line>
					                </g>
					            </svg>
					        </a>
						    </div>
					</div>

					<div class="section m-0 mobile-nobg" style="background: url('demos/seo/images/sections/background-section-3-3.png') no-repeat center center; background-size: cover; padding: 100px 0;">
						<div class="container">
							<div class="row justify-content-between align-items-center">

								<div class="col-md-4">
									<div class="heading-block nobottomborder bottommargin-sm">
										<h3 class="nott ls0">Deposit Dengan Bank Lokal</h3>
									</div>
									<p class="text-justify">Broker Forex OctaFX menyediakan bank-bank lokal utama Indonesia untuk memudahkan proses deposit dalam mata uang lokal. Deposit dengan bank lokal yang telah kami sediakan dan pastinya bebas komisi untuk setiap deposit dan penarikan dana.</p>
								</div>

								<div class="col-lg-5 col-md-5">
									<div class="card shadow-sm">
										<div class="card-body">
											<div class="row">
												<div class="col-6 parallax" data-bottom-top="transform: translateX(-50px)" data-top-bottom="transform: translateX(50px)">
													<img src="demos/seo/images/sections/bca.png" alt="">
													<img src="demos/seo/images/sections/bni.png" alt="">
													<img src="demos/seo/images/sections/bri.png" alt="">
												</div>
												<div class="col-6 parallax" data-bottom-top="transform: translateX(50px)" data-top-bottom="transform: translateX(-50px)">
													<img src="demos/seo/images/sections/mandiri.png" alt="">
													<img src="demos/seo/images/sections/btc.png" alt="">
													<img src="demos/seo/images/sections/cpay.png" alt="">
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="hide-mobile col-md-3 mt-5 mt-md-0 center">
									<a href="https://www.youtube.com/watch?v=LXZgliAowQ4" data-lightbox="iframe" class="play-icon shadow"><i class="icon-play"></i></a>
								</div>

							</div>

						</div>
					</div>

					<div class="container clearfix py-5">
						<div class="row">
							<div class="heading-block nobottomborder center divcenter mb-0 clearfix">
								<h3 class="nott ls0 mb-3">Mengapa memilih Trading Forex dengan OctaFX?</h3>
								<p class="text-justify">Broker Forex OctaFX memastikan kondisi trading Forex yang handal untuk menyediakan kepada para trader dari semua tingkatan dengan peluang untuk untung lebih banyak. Ini bisa terjadi karena:
								</p>
							</div>
							<div class="col-md-3">
								<div class="feature-box fbox-center noborder">
									<div class="fbox-icon">
										<a href="#"><img src="demos/seo/images/icons/content_marketing.svg" alt="Feature Icon" class="nobg noradius"></a>
									</div>
									<h3 class="nott ls0">Tidak ada komisi pada tiap deposit dan penarikan dana</h3>
								</div>
							</div>

							<div class="col-md-3">
								<div class="feature-box fbox-center noborder">
									<div class="fbox-icon">
										<a href="#"><img src="demos/seo/images/icons/growth.svg" alt="Feature Icon" class="nobg noradius"></a>
									</div>
									<h3 class="nott ls0">Tidak ada swap</h3>
								</div>
							</div>

							<div class="col-md-3">
								<div class="feature-box fbox-center noborder">
									<div class="fbox-icon">
										<a href="#"><img src="demos/seo/images/icons/analysis.svg" alt="Feature Icon" class="nobg noradius"></a>
									</div>
									<h3 class="nott ls0">Tidak ada slippage</h3>
								</div>
							</div>

							<div class="col-md-3">
								<div class="feature-box fbox-center noborder">
									<div class="fbox-icon">
										<a href="#"><img src="demos/seo/images/icons/seo.svg" alt="Feature Icon" class="nobg noradius"></a>
									</div>
									<h3 class="nott ls0">Tidak ada terlambat</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- News
				============================================= -->
				<div class="section m-0" style="background: url('demos/seo/images/sections/bg-stock-gray-left.jpg') no-repeat center top; background-size: cover; padding: 0px 0 0;">
					<div class="container">
						<div class="heading-block nobottomborder center">
							<h3 class="nott ls0">Berita Terbaru</h3>
						</div>

						<div id="portfolio" class="portfolio portfolio-3 grid-container clearfix">

							<div>
								<article class="portfolio-item pf-media pf-icons">
								<div class="portfolio-image">
									<img src="https://www.octafx.forex/pics/a/Market%20Insights/Weekly%20Forecast/2019/13-05-19WF.png" alt="The Atmosphere">
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="#">Negosiasi Perdagangan Dan Tajuk Utama Penerbitan Data Makroekonomi Amerika Serikat</a></h3>
									<span>Prakiraan Mingguan</span>
								</div>
							</article>

							<article class="portfolio-item pf-illustrations">
								<div class="portfolio-image">
									<img src="https://www.octafx.forex/pics/a/Market%20Insights/Weekly%20Forecast/2019/13-05-19WF.png" alt="Wavelength Structure">
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3>Perang Dagang Memanas Begitu Amerika Serikat Menerapkan Tarif Perdagangan Baru Terhadap China</h3>
									<span>Prakiraan Mingguan</span>
								</div>
							</article>

							<article class="portfolio-item pf-graphics pf-uielements">
								<div class="portfolio-image">
									<img src="https://lh4.googleusercontent.com/NQEVwliwCZZ_EEQ94VAMlMp3yIplebBv2K6XoK5Qk_aPgK3JXeESjh4w7Tmw6K1UmpcSV0uzfELQDww0jFhgcjz-xQnzyhaTyywJ8XQr1L1qs5kA97x45FnDb-b_ZbMOp5BMzSGzMn2qfQNcSw" alt="Greenhouse Garden">
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3>EURUSD 1.1165 Level Jeda</h3>
									<span>Wawasan Market</span>
								</div>
							</article>
							</div>

							<div>
								<article class="portfolio-item pf-icons pf-illustrations">
								<div class="portfolio-image">
									<img src="https://lh4.googleusercontent.com/NQEVwliwCZZ_EEQ94VAMlMp3yIplebBv2K6XoK5Qk_aPgK3JXeESjh4w7Tmw6K1UmpcSV0uzfELQDww0jFhgcjz-xQnzyhaTyywJ8XQr1L1qs5kA97x45FnDb-b_ZbMOp5BMzSGzMn2qfQNcSw" alt="Industrial Hub">
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3>USDJPY Tertolak Dari 110.00</h3>
									<span>Wawasan Market</span>
								</div>
							</article>

							<article class="portfolio-item pf-uielements pf-media">
								<div class="portfolio-image">
									<img src="https://editorial.azureedge.net/miscelaneous/USD_CAD%20(8)-636936983532904707.png" alt="Corporate Headquarters">
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3>AS Dan Kanada Hampir Sepakat Untuk Hapus Tarif Baja Dan Alumunium - CBC</h3>
									<span>Berita Forex</span>
								</div>
							</article>

							<article class="portfolio-item pf-graphics pf-illustrations">
								<div class="portfolio-image">
									<img src="https://editorial.azureedge.net/miscelaneous/USD_CAD%20(8)-636936983532904707.png" alt="Space Station">
									<div class="portfolio-overlay" data-lightbox="gallery">
										<a href="#" class="center-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3>Analisa Teknis USD/CAD: Bulls Bersiap Untuk Akhirnya Menembus Kisaran Perdagangan 4-Minggu</h3>
									<span>Berita Forex</span>
								</div>
							</article>
							</div>

						</div>

						<div class="center mb-5">
							<a href="#" class="button button-large button-hover button-rounded capitalize ml-0 mt-5 ls0">Lihat Semua Berita</a>
						</div>

					</div>

				</div>

				<!-- Events -->
				<div class="section mt-0 pt-0 mb-0 pb-0">
					<div class="content-wrap content-wrap mb-0 pb-0">
						<div class="parallax header-stick bottommargin-lg dark" style="padding: 60px 0; background-image: url('demos/seo/images/sections/2.jpg'); height: auto;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -80px;">
							<div class="container clearfix">
								<div class="events-calendar">
									<div class="events-calendar-header clearfix">
										<h2>OctaFX Explorer - Jadwal Seminar</h2>
										<h3 class="calendar-month-year">
										<span id="calendar-month" class="calendar-month"></span>
										<span id="calendar-year" class="calendar-year"></span>
										<nav>
											<span id="calendar-prev" class="calendar-prev"><i class="icon-chevron-left"></i></span>
											<span id="calendar-next" class="calendar-next"><i class="icon-chevron-right"></i></span>
											<span id="calendar-current" class="calendar-current" title="Got to current date"><i class="icon-reload"></i></span>
										</nav>
										</h3>
									</div>
									<div id="calendar" class="fc-calendar-container"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Testimonials
				============================================= -->
				<div class="section mt-0 pt-0 mb-0 pb-0 promo-section ">
					<div class="container">
						<div class="heading-block nobottomborder center">
							<h3 class="nott ls0">Inilah Tanggapan Mereka</h3>
						</div>

						<div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget clearfix" data-margin="0" data-pagi="true" data-loop="true" data-center="true" data-autoplay="5000" data-items-sm="1" data-items-md="2" data-items-xl="3">

							<div class="oc-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="https://www.octafx.forex/assets/pic/testimonials-pic/testimonials-pic-01.jpg?9b7359e3150b5fdd03d173f7ffbf4f2a94717d8b" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Broker terbaik, spread yang rendah, saya sangat menyukainya, trading dan withdrawal yang cepat</p>
										<div class="testi-meta">
											Mohamed Elamine Mekki
											<span>Algeria</span>
										</div>
									</div>
								</div>
							</div>

							<div class="oc-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="https://www.octafx.forex/assets/pic/testimonials-pic/testimonials-pic-04.jpg?beeb2ef6cc566bb6840d8cd230a75da064f0a33f" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Saya bergabung dengan OctaFX sejak 2014. Broker ini sangat bagus dalam hal kecepatan eksekusi, no requote, dan spread sangat kecil. Keunggulan lainya OctaFX bisa deposit dan withdraw dengan lokal bank Indonesia, proses depo wd pun sangat cepat</p>
										<div class="testi-meta">
											Eko Bagus Setiawan
											<span>Indonesia</span>
										</div>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="https://www.octafx.forex/assets/pic/testimonials-pic/testimonials-pic-03.jpg?50337150a49ca70e9b4673d4ff8086841172fd5f" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>OctaFX adalah broker yang layak - baik untuk semua trader: baik itu pemula dan yang berpengalaman. Banyak pilihan pasangan mata uang dan di sini saya selalu mendapatkan profit</p>
										<div class="testi-meta">
											Mostafa Kamal
											<span>Bangladesh</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Awards
				============================================= -->
				<div class="section m-0 pt-0">
					<div class="container-award">
						<div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-margin="100" data-loop="true" data-autoplay="5000" data-nav="false" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="7" data-items-xl="7">
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-07.svg?a8a0d91c3d4d1daae9693a6b7ad99cfbd681498a" alt="Brands">
								<center>
									<span class="award-slide">BROKER FOREX TERBAIK SE-ASIA 2018 <br>- GLOBAL BANKING AND FINANCE REVIEW
										<br>
										<br>
									</span>
									<strong>2018</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-06.svg?b3499b2ff0f0bd8c5bc1bc244402fadeb0ce380c" alt="Brands">
								<center>
									<span class="award-slide">
										BEST FOREX ECN BROKER <br>
										-UK FOREX AWARDS
										<br>
										<br>
										<br>
									</span>
									<strong>2017</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-01.svg?5a3bd370f6e9cb570f0852a7ba758af9b2af91db" alt="Brands">
								<center>
									<span class="award-slide">
										BEST TRADING CONDITIONS FOREX <br>
										- REPORT MAGAZINE
										<br>
										<br>
										<br>
									</span>
									<strong>2016</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-02.svg?2da6cfad743032647c235a7a9b965091203a8352" alt="Brands">
								<center>
									<span class="award-slide">
										BEST ISLAMIC ACCOUNT FOREX<br>
										- BROKER FOREXTRADERS
										<br>
										<br>
										<br>
									</span>
									<strong>2015</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-03.svg?26bfa2e897efb8a5384e64b40a1b2ac6b1d629aa" alt="Brands">
								<center>
									<span class="award-slide">
										BEST ECN BROKER<br>
										- ASIA GLOBAL BANKING AND FINANCE REVIEW
										<br>
										<br>
									</span>
									<strong>2014</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-04.svg?4dc8401861e2ab9d950a25665c4ea389b403b84b" alt="Brands">
								<center>
									<span class="award-slide">
										BEST CUSTOMER SERVICE BROKER<br>
										-  FX EMPIRE
										<br>
										<br>
										<br>
									</span>
									<strong>2013</strong>
								</center>
							</div>
							<div class="oc-item">
								<img src="https://www.octafx.forex/assets/img/common/award/award-05.svg?7ab8431801f6cff0ae38b2c2c2144bc07f854577" alt="Brands">
								<center>
									<span class="award-slide">
										FASTEST GROWING MICRO FOREX BROKER<br>
										- GLOBAL BANKING AND FINANCE REVIEW
										<br>
									</span>
									<strong>2012</strong>
								</center>
							</div>
						</div>
					</div>
				</div>

				<!-- Promo/Contact
				============================================= -->
				<div class="section m-0 p-0 footer-stick nobg" style="padding: 100px 0; overflow: visible">
					<div class="container">
						<div class="heading-block nobottomborder center">
							<h2 class="nott ls0">Ayo Segera Mulai Bisnis Baru Anda <span>Hari Ini</span></h2>
							<a href="#myModal1" data-lightbox="inline" class="button button-large button-rounded nott ml-0 ls0 mt-4"><i class="icon-phone3"></i> Telpon Kembali</a>
						</div>
					</div>
				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="noborder bg-white">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights" style="background: url('demos/seo/images/hero/footer.svg') no-repeat top center; background-size: cover; padding-top: 70px;">

				<div class="container clearfix">

					<div class="row">
						<div class="col-12 col-md-6 col-lg-6 text-white">
						Lisensi bisnis dengan nomor 19776 IBC 2011 dan Alamat registrasi: Suite 305, Griffith Corporate Centre, Beachmont, Kingstown, St. Vincent and the Grenadines.<br>
					</div>

					<div class="col-12 col-md-6 col-lg-6 tright">
						<div class="copyrights-menu copyright-links clearfix text-white">
							<div class="copyright-links text-white hide-mobile">
								<a href="#" class="text-white">ISI WEBSITE</a> / <a href="#" class="text-white">PEMBERITAHUAN RISIKO</a>
								<a href="#" class="text-white">KEBIJAKAN PRIVASI</a> / <a href="#" class="text-white">KEBIJAKAN PENGEMBALIAN</a>
								<a href="#" class="text-white">CUSTOMER AGREEMENT</a> / <a href="#" class="text-white">KEBIJAKAN AML</a>
							</div>
							<div class="mobile-copyright mobile-mt-1">Copyrights &copy; 2011-2019 Octa Markets Incorporated<br></div>
						</div>
					</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/jquery.calendario.js"></script>
	<script>
		var octaFXSchedule = {
			'02-05-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-jakarta" target=_blank>JAKARTA</a>',
			'02-26-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-tanjungpandan-belitung" target=_blank>TANJUNGPANDAN BELITUNG</a>',
			'03-16-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-surabaya" target=_blank>SURABAYA</a>',
			'03-26-2019' : '<a href="https://gallery.octafx.show/roadshow/pemenang-motor-mingguan-octafx-ridwan-hidayat-bandung" target=_blank>PEMENANG MOTOR MINGGUAN OCTAFX - RIDWAN</a>',
			'04-19-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-balikpapan" target=_blank>BALIKPAPAN</a>',
			'04-27-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-solo" target=_blank>SOLO</a>',
			'05-12-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-medan" target=_blank>MEDAN</a>',
			'05-31-2019' : '<a href="https://gallery.octafx.show/roadshow/octafx-explorer-pontianak" target=_blank>PONTIANAK</a>'}
	</script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>

</body>
</html>